"""
Game file. Shows all game menus, runs all the game logic.
"""
from PyInquirer import prompt, Validator, ValidationError
from .models import Player, Game, Hand
import names
from beautifultable import BeautifulTable
import pyfiglet


class RoundsValidator(Validator):
    def validate(self, document):
        try:
            rounds = int(document.text)
            if not 0 <= rounds <= 99:
                raise Exception
        except Exception:
            raise ValidationError(message='Please enter valid rounds quanity - number between 0 and 99', cursor_position=len(document.text))


name_dialog = [
    {
        'type': 'input',
        'name': 'player_name',
        'message': 'What is your name, wanderer?',
    }    
]


game_menu = [
    {
        'type': 'list',
        'name': 'then',
        'message': 'Ok! Shall we?',
        'choices': ['Start new game', 'Exit'],
    }    
]


rounds_dialog = [
    {
        'type': 'input',
        'name': 'rounds',
        'message': 'Enter rounds number, for exit keep it 0',
        'default': '0',
        'validate': RoundsValidator
    }
]


symbols_dialog = [
    {
        'type': 'list',
        'name': 'symbol',
        'message': 'Choose your symbol',
        'choices': Hand.symbol_list + ['Exit']
    }    
]


another_game_dialog = [
    {
        'type': 'confirm',
        'name': 'another_game',
        'message': 'Let us have another game?',
        'default': False
    },
]


round_result_str = {0: 'Draw', 1: 'Win', -1: 'Lose'}


def draw_score_table(data: dict, player1_name: str, player2_name: str) -> str:
    """Create pretty score table
    
    Arguments:
        data {dict} -- scores dict
        player1_name {str} -- player1 name
        player2_name {str} -- player2 name
    
    Returns:
        str -- score table string representation in pretty form
    """
    if len(data) > 0:
        table = BeautifulTable()
        table.column_headers = ['round', player1_name, player2_name]
        final_sum = 0
        for row in data.items():
            table.append_row([row[0], round_result_str[row[1]], round_result_str[0-row[1]]])
            final_sum += row[1]
        return table
    return None


def run():
    """ Running the game """
    
    # Empty name means that player has never yet played
    name = ''
    while True:
        if name == '':
            name = prompt(name_dialog).get('player_name')
            name = name if len(name) > 0 else 'Captain Nemo'
        
        opponents_name = names.get_first_name()
        
        # starting the game
        # enter rounds number
        rounds_dialog[0].update(
            {'message': 'Ok, {}! Your opponent is {}! How many rounds you wanna play?! (keep 0 to exit to main menu)'.format(name, opponents_name)}
        )
        rounds = int(prompt(rounds_dialog).get('rounds', 3))
        if rounds == 0:
            break
        
        # initializing players and game objects
        human = Player(name)
        computer = Player(opponents_name)
        game = Game(human, computer, rounds)
        
        # OK! The game begins here
        while not game.over():
            # showing symbols dialog
            symbol = prompt(symbols_dialog).get('symbol')
            if symbol == 'Exit':
                break
            human.hand.symbol = symbol
            computer.hand.symbol = Hand.get_random_symbol()
            
            print(
                '{}! You got {}, {} got {}'.format(
                    round_result_str.get(game.fire_round()),
                    human.hand.symbol,
                    computer.name,
                    computer.hand.symbol
                )
            )
        
        print('\nSo, here\'s a result:\n', draw_score_table(game.tour_table, human.name, computer.name))
        # Showing the final score
        if game.score == 0:
            print('It is draw, noone wins')
        else:
            if game.score > 0:
                print(pyfiglet.figlet_format("YOU WIN!!!"))
            else:
                print(pyfiglet.figlet_format("you lose...sorry :("))
        print('')
        
        # Have another game?
        if not prompt(another_game_dialog).get('another_game'):
            break