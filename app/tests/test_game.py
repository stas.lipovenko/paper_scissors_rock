from unittest import TestCase
from engine.models import Player, Game, GameException

class TestGame(TestCase):
    game = None
    
    def setUp(self):
        self.player1 = Player('name1')
        self.player2 = Player('name2')
        rounds = 3
        self.game = Game(self.player1, self.player2, rounds)
        
    def tearDown(self):
        self.player1 = None
        self.player2 = None
        self.game = None
        
    def test_case_01(self):
        """ test exception when creating game with invalid data """
        with self.subTest():
            with self.assertRaises(GameException):
                Game(
                    None,
                    Player('name'),
                    3
                )
                
        with self.subTest():
            with self.assertRaises(GameException):
                Game(
                    Player('name'),
                    None,
                    3
                )
                
        with self.subTest():
            with self.assertRaises(GameException):
                Game(
                    Player('name1'),
                    Player('name2'),
                    0
                )
                
    def test_case_02(self):
        """ test game over feature """
        with self.subTest():
            self.assertFalse(self.game.over())
            
        with self.subTest():
            self.game._Game__rounds = 0
            self.assertTrue(self.game.over())
            
    
    def test_case_03(self):
        """ perform game simulation - player1 wins"""
        self.player1.hand.symbol = 'Paper'
        self.player2.hand.symbol = 'Rock'
        self.assertEqual(1, self.game.fire_round())
        
        
    def test_case_04(self):
        """ perform game simulation - player1 loses"""
        self.player1.hand.symbol = 'Rock'
        self.player2.hand.symbol = 'Paper'
        self.assertEqual(-1, self.game.fire_round())
        
        
    def test_case_05(self):
        """ perform game simulation - draw"""
        self.player1.hand.symbol = 'Rock'
        self.player2.hand.symbol = 'Rock'
        self.assertEqual(0, self.game.fire_round())

    def test_case_06(self):
        """ perform game simulation - exception is raised when trying to fire round but game is over"""
        self.player1.hand.symbol = 'Rock'
        self.player2.hand.symbol = 'Paper'
        for _ in range(self.game._Game__rounds):
            _ = self.game.fire_round()
        with self.assertRaises(GameException):
            _ = self.game.fire_round()
