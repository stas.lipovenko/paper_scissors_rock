from random import choice
from collections import OrderedDict

class GameException(ValueError):
    """ 
    Just a wrap for exception
    """
    pass


class Hand:
    """ class representing player's hand and symbols it can throw """
    
    # Available hand symbols
    symbol_list = ['Rock', 'Scissors', 'Paper']
    
    # just form emuating computer player
    @classmethod
    def get_random_symbol(cls) -> str:
        """Returns randomly picked hand symbol from the symbol_list
        
        Returns:
            str -- string hand symbol
        """
        return choice(cls.symbol_list)

    # initially hand doesn't hold sany symbol
    def __init__(self):
        self.__symbol = None
        
    @property
    def symbol(self):
        return self.__symbol
    
    @symbol.setter
    def symbol(self, new_symbol: str):
        """Sets new symbol to player's hand, check if symbol is valid
        
        Arguments:
            new_symbol {str} -- symbol to be set to hand
        
        Raises:
            GameException: No such hand symbol available
        """
        if new_symbol in Hand.symbol_list:
            self.__symbol = new_symbol
        else:
            raise GameException('No such hand symbol available: {}'.format(new_symbol))
    
    # checks if two hand have equal symbols
    def __eq__(self, other) -> bool:
        """
        magic method to override equality check between to hands
                
        Arguments:
            self {Hand} -- Hand object to compare
            other {Hand} -- Hand object to be compared to
        
        Returns:
            bool -- True if symbols are equal, False - otherwise
        """
        return self.symbol == other.symbol
    
    # greater hand means that it holds symbol that beats other's symbol
    def __gt__(self, other) -> bool:
        """
        magic method to override 'greater than' check between to hands
        
        Arguments:
            self {Hand} -- Hand object to compare
            other {Hand} -- Hand object to be compared to
        
        Returns:
            bool -- True if self Hand object's symbol has more power above other Hand object's symbol
        """
        is_win = (
            (self.symbol == 'Rock' and (other.symbol == 'Scissors'))
            or
            (self.symbol == 'Scissors' and (other.symbol == 'Paper'))
            or 
            (self.symbol == 'Paper' and (other.symbol == 'Rock'))
        )
        return is_win
  

class Player:
    """ class representing player in the game. Player has hand and can hold a symbol in it """
    
    def __init__(self, name):
        self.__name = name
        self.__hand = Hand()
        
    @property
    def name(self):
        return self.__name
    
    # this seems fair enough
    @name.setter
    def name(self, name):
        raise GameException('You can\'t change player\'s name within a game')

    @property
    def hand(self):
        """Descriptor for player's hand
        
        Returns:
            Hand
        """
        return self.__hand


class Game:
    """
    class representing game. Game has to players and hold the rounds quantinty 
    Also produces game statistics roundwise
    """
    def __init__(self, human: Player, computer: Player, rounds: int):
        """ initializing game
        
        Arguments:
            human {Player} -- player controlled by human
            computer {Player} -- player controlled by computer
            rounds {int} -- rounds quantity
        
        Raises:
            GameException: raises if passed argeuments are invalid
        """
        try:
            human.name
            computer.name
        except AttributeError:
            raise GameException('Both human and computer must be of Player type')
        
        if rounds <= 0:
            raise GameException('Cannot start the game with 0 rounds')
        self.__human = human
        self.__score = 0

        self.__computer = computer
        
        # variables to maintain game process and stats
        self.__rounds = rounds
        self.__current_round = 1
        self.__tour_table = OrderedDict()
        
    def fire_round(self) -> int:
        """ fires the round, compares two players' hands and decides who won this time. Uses hand's magic methods
            stores the stats and score
        
        Returns:
            int -- returns 0 if it is draw, otherwise 1: human wins, -1 human loses
        
        Raises:
            GameException: raised when game is over
        """
        if self.__current_round > self.__rounds:
            raise GameException('Game over!')
        
        if self.__human.hand == self.__computer.hand:
            result = 0
        else:
            result = 1 if self.__human.hand > self.__computer.hand else -1
        
        # storring statistics and score
        self.__tour_table[self.__current_round] = result
        self.__score += result
        
        # increase round number
        self.__current_round += 1
        return result
    
    def over(self) -> bool:
        """Checks if the game is over
        
        Returns:
            bool -- True if over, False otherwise
        """
        return self.__current_round > self.__rounds

    @property
    def score(self):
        return self.__score
    
    @property
    def tour_table(self):
        return self.__tour_table