from unittest import TestCase
from engine.models import Player, Hand, GameException

class TestPlayer(TestCase):
    player = None
    
    @classmethod
    def setUpClass(cls):
        cls.player = Player('Nemo')
        
    @classmethod
    def tearDownClass(cls):
        cls.player = None
        
    def test_case_01(self):
        """ test exception when trying to change player's name """
        with self.assertRaises(GameException):
            self.player.name = 'Black Beard'
                
    def test_case_02(self):
        """ test setting hand symbol for player """
        symbol = 'Paper'
        self.player.hand.symbol = symbol
        self.assertEqual(symbol, self.player.hand.symbol)
