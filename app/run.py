"""
Main starter file.
Shows intro and starts the Game
"""

from PyInquirer import prompt, Validator, ValidationError
from pyfiglet import Figlet
from engine import game
from time import sleep

main_menu = [
    {
        'type': 'list',
        'name': 'main_menu',
        'message': 'Ok! Shall we?',
        'choices': ['Start new game', 'Exit'],
    }    
]

if __name__ == '__main__':
    while True:
        f = Figlet(font='slant')
        print(f.renderText('Rock'))
        sleep(0.4)
        print(f.renderText('Scissors'))
        sleep(0.4)
        print(f.renderText('Paper'))
        sleep(0.4)
        print(f.renderText('GAME!'))
        
        answer = prompt(main_menu).get('main_menu', 'Exit')
        if answer == 'Exit':
            break
        # starting
        game.run()