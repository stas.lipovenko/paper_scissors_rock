from unittest import TestCase
from engine.models import Hand, GameException

class TestHand(TestCase):
    hand1 = None
    hand2 = None
    
    @classmethod
    def setUpClass(cls):
        cls.hand1 = Hand()
        cls.hand2 = Hand()
        
    @classmethod
    def tearDownClass(cls):
        cls.hand1 = None
        cls.hand2 = None
        
    def test_case_01(self):
        """ test setting valid symbol to hand """
        symbol = 'Rock'
        self.hand1.symbol = symbol
        self.assertEqual(self.hand1.symbol, symbol)
        
    def test_case_02(self):
        """ test exception raises when setting invalid symbol to hand """
        symbol = 'Invalid'
        with self.assertRaises(GameException):
            self.hand1.symbol = symbol

    def test_case_03(self):
        """ test relations between two hands """
        self.hand1.symbol = 'Rock'
        with self.subTest():
            self.hand2.symbol = 'Scissors'
            self.assertGreater(self.hand1, self.hand2)

        with self.subTest():
            self.hand2.symbol = 'Paper'
            self.assertGreater(self.hand2, self.hand1)
            
        with self.subTest():
            self.hand2.symbol = 'Rock'
            self.assertEqual(self.hand1, self.hand2)
